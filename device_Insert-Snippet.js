







// *** Common ***

// Device login hook stub
function deviceLoginHook(inputs, actions) {
    /*
    Use Device Scripting to implement copy, scan, and fax policies at the MFD.
    You don't have to be a programmer as there are many pre-written recipes available for you to use.
    But if you're confident, you can write your own scripts in JavasScript using snippets and the reference documentation.
    To implement Printing policies, try out our Print Scripting.
    */
    // your script here
}
  


// Device job log hook stub
function deviceJobLogHook(inputs, actions) {
    /*
    This script is run when PaperCut receives a job log from the device.
    In other words, a user has just completed a copy/fax/scan job on a device and the job log has been passed to PaperCut for tracking / accounting purposes.
    All code is written in JavaScript, and prior experience with scripting is assumed.
    Use the provided recipes, snippets and reference documentation to assist with script development.
    */
    // your code here
}
  


// Device print release hook stub
function devicePrintReleaseHook(inputs, actions) {
    /*
    This script is run when a user attempts to release a held print job on the device.
    This hook can be used to block a user from releasing a print job on the device but it will not block the job if the user releases it via another release interface (mobile print release, release station). In other words, reconsider the use of this hook if there are multiple release interfaces as a user will be able to bypass this hook.
    All code is written is written in JavaScript, and prior experience with scripting is assumed.  Use the provided recipes, snippets and reference documentation to assist with script development.
    */
    // your code here
}



// ** Tests **

// Test user group
// Test if a user is in group called 'students'.
if (inputs.user.isInGroup("students")) {
    // Add your action code here
    actions.log.debug("The user is in 'students' group.");
}
  
  

// Test device group
    // Test if a device belongs to 'Department: Science' device group.
if (inputs.device.isInGroup("Department: Science")) {
    // Add your action code here
    actions.log.debug("The device is in the 'Department: Science' group.");
}



// Test number of pages
    // If total pages is greater than 200 then perform action
if (inputs.job.totalPages > 200) {
    // Add your action code here
    actions.log.debug("The job has more than 200 pages");
}

  

// Test if job is color
    // Test if the job is color (contains at least 1 color page)
if (inputs.job.isColor) {
    // Add your action code here
    actions.log.debug("The job is color");
}
  
// Test if the job is Grayscale
if (inputs.job.isGrayscale) {
    // Add your action code here
    actions.log.debug("The job is grayscale");
}
  
  

// Test if it is a copy job
    // Test if the job is a copy job (as opposed to scan/fax)
if (inputs.job.isCopy) {
    // Add your action code here
    actions.log.debug("This is a copy job!");
}



// Test if job is duplex
    // Test if the job is duplex (double-sided)
if (inputs.job.isDuplex) {
    // Add your action code here
    actions.log.debug("The job is double-sided");
}



// Test job cost
    // Test if the job costs more than $10.00
if (inputs.job.cost > 10.0) {
    // Add your action code here
    actions.log.debug("The job costs more than $10.00");
}



// Test job time (hour)
// Test if the job was done between 6:00pm and 11:00pm noninclusive.
var hour = inputs.job.date.getHours();  // In 24 hour time
if (hour >= 18 && hour < 23) {
    // Add your action code here
    actions.log.debug("This job was done between 6 and 11pm");
}



// Test job time (day)
    // Test if the job was performed after the 15th of the month
var dayOfMonth = inputs.job.date.getDate();  // from 1 - 31
if (dayOfMonth > 15) {
    // Add your action code here
    actions.log.debug("This job was performed after the 15th");
}




// *** User Interaction ***
// send an email to a recipient (can be an email address or username)
    // Hint - to send to multiple recipients, use an array:
    // eg. ["andy", "simon", "support@mail.com"]
    var recipient = "recipient@emailaddress.com";
    var subject = "insert email subject here";
    var body = "insert email body here";
actions.utils.sendEmail(recipient, subject, body);




// *** Actions ***

// Bypass login filters and restrictions
    // Bypass any login filters and restrictions and grant the user device login access.
    // Example: Grant students login access to a copier between 2pm and 4pm
var date = new Date();
var hours = date.getHours();
if (inputs.user.isInGroup("Students") && (hours >= 14 && hours < 16)) {
    actions.login.bypassLoginFilters();
}



// Deny login access
    // Deny device login to students before 8am or after 6pm.
var date = new Date();
var hours = date.getHours();
if (inputs.user.isInGroup("Students") && (hours < 8 || hours >= 18)) {
    actions.login.denyLoginAccess();
}



// Bypass color copy filters and restrictions
    // allow a user to bypass any color copy filters & restrictions
actions.login.bypassColorCopyFilters();



// Deny color copy access
    // deny color copy on login
actions.login.denyColorCopyAccess();



// Restrict selectable shared accounts
    // Limit the accounts available to the user at the "Account Selection" screen.
    // Users will still need the appropriate User/Group permissions to see these accounts.
actions.login.setAvailableSharedAccounts(['Biology', 'Chemistry', 'Physics']);



// Add a comment to the job
    // Add a job comment. This will be listed in the print/device log and associated log reports.
if (inputs.job.totalPages > 100) {
    actions.job.addComment("Jobs with over 100 pages deserve a comment!");
}



// Change the job cost
    // 20% volume discount for jobs with more than 100 pages
if (inputs.job.totalPages > 100) {
    var newCost = inputs.job.cost * 0.8;
    actions.job.setCost(newCost);
    actions.job.addComment('20% volume discount applied');
    actions.log.debug('Applied 20% volume discount to job from user "' +
        inputs.user.username + '."');
}



// Block user from releasing a held print job
    // block a user from releasing a held job
    // and provide a custom message as to why the job was denied
actions.heldJob.blockRelease("Held job(s) denied due to release restrictions. Modify your job and try again.");




// *** Properties ***

// Save/Set a user property
    // Sets a user property.
    // NOTE: This operation is not performed until the device hook
    // has finished running.
actions.user.onCompletionSaveProperty("my-property-name", "My Value");
    // Get a user property
    // Gets a user property, or return null if does not exist.
var myUserProp = inputs.user.getProperty("my-property-name");
if (myUserProp == null) {
    myUserProp = "my-default-value";
}




// Increment a user number property
    // Increment a numerical user property by some amount.
    // NOTE: This operation is not performed until the device hook
    // has finished running.
actions.user.onCompletionIncrementNumberProperty("my-property-name", 10);



// Save/Set a device property
    // Sets a global property.
    // Sets a device property.
    // NOTE: This operation is not performed until the device hook
    // has finished running.
actions.device.onCompletionSaveProperty("my-property-name", "My Value");



// Get a device property
    // Gets a device property, or return null if does not exist.
var myDeviceProp = inputs.device.getProperty("my-property-name");
if (myDeviceProp == null) {
    myDeviceProp = "my-default-value";
}



// Increment a device number property
    // Increment a numerical device property by some amount.
    // NOTE: This operation is not performed until the device hook
    // has finished running.
actions.device.onCompletionIncrementNumberProperty("my-property-name", 10);



// Save/Set a global property
    // Sets a global property.
    // NOTE: This operation is not performed until the device hook
    // has finished running.
actions.utils.onCompletionSaveProperty("my-property-name", "My Value");



// Get a global property
    // Gets a global property, or return null if does not exist.
var myGlobalProp = inputs.utils.getProperty("my-property-name");
if (myGlobalProp == null) {
    myGlobalProp = "my-default-value";
}



// Increment a global number property
    // Increment a numerical global property by some amount.
    // NOTE: This operation is not performed until the device hook
    // has finished running.
actions.utils.onCompletionIncrementNumberProperty("my-property-name", 10);



// *** Utilities ***

// Log a message to the App. Log
    // These messages will appear in the App. Log
actions.log.info("This is an info level message.");
actions.log.warning("This is an warning level message and will show in yellow.");
actions.log.error("This is an error level message and will show in red."
+ "It may also trigger an email alert if set up.");



// Log a debug message to server.log
    // Log debug message to [install-path]/server/logs/server.log
actions.log.debug("This is a debug level message");



// Format a balance
    // Format the user's balance
var balanceFormatted = inputs.utils.formatBalance(inputs.user.balance);
actions.log.debug("The user's current balance is " + balanceFormatted);



// Format a cost
    // Format the job cost with locale specific currency format
var formattedCost = inputs.utils.formatCost(inputs.job.cost);
actions.log.debug("The job's cost is: " + formattedCost);


